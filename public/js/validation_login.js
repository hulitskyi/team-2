let submitBtn = document.querySelector('.login-main__btn')
let passwordField = document.getElementById('Password');
let emailField = document.getElementById('Email');

function validate(str) {
    let re = /[A-Za-z0-9]{3,}/g;
    return re.test(str);
}


submitBtn.addEventListener('click', () => {
    let isValid = true;

    if (!validate(passwordField.value)) {
        passwordField.classList.add('invalid');
        document.getElementById('passwordAdvice').innerHTML = 'Field is blank or has less than 3 characters';
        isValid = false;
    } else {
        passwordField.classList.remove('invalid');
        document.getElementById('passwordAdvice').innerHTML = '';
    }
    if (!validate(emailField.value)) {
        emailField.classList.add('invalid');
        document.getElementById('emailAdvice').innerHTML = 'Field is blank or has less than 3 characters';
        isValid = false;
    } else {
        emailField.classList.remove('invalid');
        document.getElementById('emailAdvice').innerHTML = '';
    }
})



passwordField.addEventListener('input', () => {
    if (!validate(passwordField.value)) {
        passwordField.classList.add('invalid');
        document.getElementById('passwordAdvice').innerHTML = 'Field is blank or has less than 3 characters';
    } else {
        passwordField.classList.remove('invalid');
        document.getElementById('passwordAdvice').innerHTML = '';
    }
});
emailField.addEventListener('keydown', () => {
    if (!validate(emailField.value)) {
        emailField.classList.add('invalid');
        document.getElementById('emailAdvice').innerHTML = 'Field is blank or has less than 3 characters';
    } else {
        emailField.classList.remove('invalid');
        document.getElementById('emailAdvice').innerHTML = '';
    }
});