let changeBtn = document.getElementById('updatePersInfor');
let fieldFirstName =document.getElementById('editFirstName');
let fieldLastName =document.getElementById('editLastName');
let fieldEmail = document.getElementById('editEmail');
let fieldUserName = document.getElementById('editUserName');
let fieldDate = document.getElementById('editDateOfBirth');
let fieldBio = document.getElementById('editBio');

//flags
let flagsValidation = {
    frn: true,
    lsn: true,
    eml: true,
    usrn: true,
    date: true,
    bio: true
};

function validateName(str) {
    let re = /^[a-zA-Z][a-zA-Z]+[a-zA-Z]?$/g;
    return (re.test(str) && str.length <= 30 && str.length >=3) || str.length === 0;
}

function validateEmail(str) {
    let re = /^\w+@\w+\.\w{2,}$/g;
    return re.test(str) || str.length === 0;
}

function validateUserName(str) {
    let re = /^[a-zA-Z0-9]([a-zA-Z]|\d)+([a-zA-Z]|\d)*?$/g;
    let re2 = /\d/g;
    return (re.test(str) && re2.test(str) && str.length <= 15 && str.length >= 3) || str.length === 0;
}

function validateData(str) {
    let re =/\d+-(0\d|1[0-2])-([0-2]\d|3[0-1])/g;
    return re.test(str)
}

function validateBio(str) {
    return str.length <= 1000 || str.length === 0;
}

//validation first name
fieldFirstName.addEventListener('input',() => {
    if (!validateName(fieldFirstName.value)) {
        fieldFirstName.classList.add('edit__invalid');
        fieldFirstName.classList.add('is-invalid');
        flagsValidation.frn = false;

    }
    else if(fieldFirstName.classList.contains('edit__invalid')) {
        fieldFirstName.classList.remove('edit__invalid');
        fieldFirstName.classList.remove('is-invalid');
        flagsValidation.frn = true;
    }
    if(!isValidateFields()) {
        changeBtn.setAttribute("disabled","disabled");
    }
    else {
        changeBtn.removeAttribute("disabled");
    }
});

//validation last name
fieldLastName.addEventListener('input',() => {
    if (!validateName(fieldLastName.value)) {
        fieldLastName.classList.add('edit__invalid');
        fieldLastName.classList.add('is-invalid');
        flagsValidation.lsn = false;
    }
    else if(fieldLastName.classList.contains('edit__invalid')) {
        fieldLastName.classList.remove('edit__invalid');
        fieldLastName.classList.remove('is-invalid');
        flagsValidation.lsn = true;
    }
    if(!isValidateFields()) {
        changeBtn.setAttribute("disabled","disabled");
    }
    else {
        changeBtn.removeAttribute("disabled");
    }
});

//validation email
fieldEmail.addEventListener('input', () => {
    if (!validateEmail(fieldEmail.value)) {
        fieldEmail.classList.add('edit__invalid');
        fieldEmail.classList.add('is-invalid');
        flagsValidation.eml = false;
    }
    else if(fieldEmail.classList.contains('edit__invalid')) {
        fieldEmail.classList.remove('edit__invalid');
        fieldEmail.classList.remove('is-invalid');
        flagsValidation.eml = true;
    }
    if(!isValidateFields()) {
        changeBtn.setAttribute("disabled","disabled");
    }
    else {
        changeBtn.removeAttribute("disabled");
    }
});

//validation userName
fieldUserName.addEventListener('input', () => {
    if(!validateUserName(fieldUserName.value)) {
        fieldUserName.classList.add('edit__invalid');
        fieldUserName.classList.add('is-invalid');
        flagsValidation.usrn = false;
    }
    else if(fieldUserName.classList.contains('edit__invalid')) {
        fieldUserName.classList.remove('edit__invalid');
        fieldUserName.classList.remove('is-invalid');
        flagsValidation.usrn = true;
    }
    if(!isValidateFields()) {
        changeBtn.setAttribute("disabled","disabled");
    }
    else {
        changeBtn.removeAttribute("disabled");
    }
});

//validation bio
fieldBio.addEventListener('input', () => {
    document.getElementById('edit__bio-counter').innerHTML = `${fieldBio.value.length}/1000`;
    if (!validateBio(fieldBio.value)) {
        fieldBio.classList.add('edit__invalid');
        fieldBio.classList.add('is-invalid');
        flagsValidation.bio = false;
    }
    else if(fieldBio.classList.contains('edit__invalid')) {
        fieldBio.classList.remove('edit__invalid');
        fieldBio.classList.remove('is-invalid');
        flagsValidation.bio = true;
    }
    if(!isValidateFields()) {
        changeBtn.setAttribute("disabled","disabled");
    }
    else {
        changeBtn.removeAttribute("disabled");
    }
});

//validation fieldDate
fieldDate.addEventListener('input', () => {
    if(!validateData(fieldDate.value)) {
        fieldDate.classList.add('edit__invalid');
        fieldDate.classList.add('is-invalid');
        flagsValidation.date = false;
    }
    else if(fieldDate.classList.contains('edit__invalid')) {
        fieldDate.classList.remove('edit__invalid');
        fieldDate.classList.remove('is-invalid');
        flagsValidation.date = true;
    }
    if(!isValidateFields()) {
        changeBtn.setAttribute("disabled","disabled");
    }
    else {
        changeBtn.removeAttribute("disabled");
    }
});

function isValidateFields() {
    for(let key in flagsValidation) {
        if(!flagsValidation[key]) {
            return false
        }
    }
    return true;
}