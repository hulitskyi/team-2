//tweet validation
let tweetField = document.getElementById('tweetsNewTweet');
let tweetBtn = document.querySelector('.tweets__new-tweet-btn');

function validateTweet(str) {
    return str.length <= 200;
}

tweetField.addEventListener('input', () => {
    document.getElementById('tweets__counter-tweet').innerHTML = `${tweetField.value.length}/200`;
    if(!validateTweet(tweetField.value)) {
        tweetField.classList.add('invalid-tweet');
        tweetField.classList.add('is-invalid');
        tweetBtn.setAttribute('disabled', 'disabled');
    }
    else if(tweetField.classList.contains('invalid-tweet')) {
        tweetField.classList.remove('invalid-tweet');
        tweetField.classList.remove('is-invalid');
        tweetBtn.removeAttribute('disabled');
    }
    else {
        tweetBtn.removeAttribute('disabled');
    }
    if(tweetField.value.length === 0){
        tweetBtn.setAttribute('disabled','disabled');
    }
});



