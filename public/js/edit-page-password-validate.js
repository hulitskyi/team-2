let currentPassword = document.getElementById('editCurrentPassword');
let passwordField = document.getElementById('editPassword');
let confirmPassword = document.getElementById('editConfirmPassword');
let submitBtn = (document.getElementsByClassName('edit__updatebtn'))[1];

function validatePassword(str){
    let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})/g;
    return regex.test(str)
}

function validateConfirm(){
    return (passwordField.value.trim() === confirmPassword.value.trim())
}

function validateCurrent(str){
    let regex = /^(?=.{6,})/g
    return regex.test(str)
}

submitBtn.addEventListener('click', () => {
    if(!validatePassword(passwordField.value)) {
        passwordField.classList.add('edit__invalid')
        passwordField.classList.add('is-invalid')
    }
    if(!validateConfirm()) {
        confirmPassword.classList.add('edit__invalid')
        confirmPassword.classList.add('is-invalid')
    }
    if(!validateCurrent(currentPassword.value)) {
        currentPassword.classList.add('edit__invalid')
        currentPassword.classList.add('is-invalid')
    }
});

passwordField.addEventListener('input', () => {
    if(!validatePassword(passwordField.value)) {
        passwordField.classList.add('edit__invalid')
        passwordField.classList.add('is-invalid')
    }else{
        passwordField.classList.remove('edit__invalid')
        passwordField.classList.remove('is-invalid')
    }
});
confirmPassword.addEventListener('input', () => {
    if(!validateConfirm()) {
        confirmPassword.classList.add('edit__invalid')
        confirmPassword.classList.add('is-invalid')
    }else{
        confirmPassword.classList.remove('edit__invalid')
        confirmPassword.classList.remove('is-invalid')
    }
});
currentPassword.addEventListener('input', () => {
    if(!validateCurrent(currentPassword.value)) {
        currentPassword.classList.add('edit__invalid')
        currentPassword.classList.add('is-invalid')
    }else{
        currentPassword.classList.remove('edit__invalid')
        currentPassword.classList.remove('is-invalid')
    }
});