let submitBtn = document.querySelector('.registr-submit__btn')
let passwordField = document.getElementById('Password');
let confirmPassword = document.getElementById('Confirm');
let dateField = document.getElementById('Date');
let emailField = document.getElementById('Email');
let userNameField = document.getElementById('Username');
let fNameField = document.getElementById('FirstName');
let lNameField = document.getElementById('LastName');

function validateData(str) {
    let regex =/\d+-(0\d|1[0-2])-([0-2]\d|3[0-1])/g;
    return regex.test(str)
}
function validatePassword(str){
    let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})/g;
    return regex.test(str)
}
function validateEmail(str) {
    let re = /[a-z\-_.]+@[a-z]+\.[a-z]+/g;
    return re.test(str);
}
function validateUsername(str) {
    let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{3,15})/g;
    return regex.test(str);
}
function validateConfirm(){
    return (passwordField.value.trim() === confirmPassword.value.trim())
}
function validateName(str) {
    let regex = /^[A-Za-z]{3,30}$/g;
    return regex.test(str);
}
submitBtn.addEventListener('click', () => {
    let isValid=true;
    if(!validateData(dateField.value)) {
        dateField.classList.add('invalid');
        document.getElementById('dateAdvice').innerHTML='incorrect date';
        isValid=false;
    }
    else{
        dateField.classList.remove('invalid');
        document.getElementById('dateAdvice').innerHTML='';
    }
    if(!validatePassword(passwordField.value)) {
        passwordField.classList.add('invalid');
        document.getElementById('passwordAdvice').innerHTML='Password must be at least six(6) characters long and must can contain one uppercase letter and number.';
        isValid=false;
    }
    else{
        passwordField.classList.remove('invalid');
        document.getElementById('passwordAdvice').innerHTML='';
    }
    if(!validateConfirm()) {
        confirmPassword.classList.add('invalid');
        document.getElementById('confirmAdvice').innerHTML='password must be the same';
        isValid=false;
    }
    else{
        confirmPassword.classList.remove('invalid');
        document.getElementById('confirmAdvice').innerHTML='';
    }
    if(!validateEmail(emailField.value)){
        emailField.classList.add('invalid');
        document.getElementById('emailAdvice').innerHTML='incorrect email';
        isValid=false;
    }
    else{
        emailField.classList.remove('invalid');
        document.getElementById('emailAdvice').innerHTML='';
    }
    if(!validateUsername(userNameField.value)){
        userNameField.classList.add('invalid');
        document.getElementById('usernameAdvice').innerHTML='Username must be at least three(3) characters long and must can contain one uppercase letter and number.';
        isValid=false;
    }
    else{
        userNameField.classList.remove('invalid');
        document.getElementById('usernameAdvice').innerHTML='';
    }
    if(!validateName(fNameField.value)){
        fNameField.classList.add('invalid');
        document.getElementById('fNameAdvice').innerHTML='incorrect First Name';
        isValid=false;
    }
    else{
        fNameField.classList.remove('invalid');
        document.getElementById('fNameAdvice').innerHTML='';
    }
    if(!validateName(lNameField.value)){
        lNameField.classList.add('invalid');
        document.getElementById('lNameAdvice').innerHTML='incorrect Last Name';
        isValid=false;
    }
    else{
        lNameField.classList.remove('invalid');
        document.getElementById('lNameAdvice').innerHTML='';
    }
    if(isValid){
        location.href='./index.html';
    }
})

dateField.addEventListener('blur', () => {
    if(!validateData(dateField.value)) {
        dateField.classList.add('invalid');
        document.getElementById('dateAdvice').innerHTML='incorrect date';
    }
    else{
        dateField.classList.remove('invalid');
        document.getElementById('dateAdvice').innerHTML='';
    }
})
dateField.addEventListener('focus', () => {
    if(dateField.classList.contains('invalid')) {
        dateField.classList.remove('invalid');
    }
});

passwordField.addEventListener('input', () => {
    if(!validatePassword(passwordField.value)) {
        passwordField.classList.add('invalid');
        document.getElementById('passwordAdvice').innerHTML='Password must be at least six(6) characters long and must can contain one uppercase letter and number.';
    }else{
        passwordField.classList.remove('invalid');
        document.getElementById('passwordAdvice').innerHTML='';
    }
});
confirmPassword.addEventListener('input', () => {
    debugger
    if(!validateConfirm()) {
        confirmPassword.classList.add('invalid');
        document.getElementById('confirmAdvice').innerHTML='Password must be the same';
    }else{
        confirmPassword.classList.remove('invalid');
        document.getElementById('confirmAdvice').innerHTML='';
    }
});
emailField.addEventListener('keydown', () => {
    if(!validateEmail(emailField.value)) {
        emailField.classList.add('invalid');
        document.getElementById('emailAdvice').innerHTML='incorrect email';
    }else{
        emailField.classList.remove('invalid');
        document.getElementById('emailAdvice').innerHTML='';
    }
});
userNameField.addEventListener('keydown', () => {
    if(!validateUsername(userNameField.value)) {
        userNameField.classList.add('invalid');
        document.getElementById('usernameAdvice').innerHTML='Username must be at least three(3) characters long and must can contain one uppercase letter and number.';
    }else{
        userNameField.classList.remove('invalid');
        document.getElementById('usernameAdvice').innerHTML='';
    }
});
fNameField.addEventListener('keydown', () => {
    if(!validateName(fNameField.value)) {
        fNameField.classList.add('invalid');
        document.getElementById('fNameAdvice').innerHTML='incorrect First Name';
    }else{
        fNameField.classList.remove('invalid');
        document.getElementById('fNameAdvice').innerHTML='';
    }
});
lNameField.addEventListener('keydown', () => {
    if(!validateName(lNameField.value)) {
        lNameField.classList.add('invalid');
        document.getElementById('lNameAdvice').innerHTML='incorrect Last Name';
    }else{
        lNameField.classList.remove('invalid');
        document.getElementById('lNameAdvice').innerHTML='';
    }
});