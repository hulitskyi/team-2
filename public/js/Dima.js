let passwordField = document.getElementById('Password');
let confirmPassword = document.getElementById('Confirm');

function validatePassword(str){
    let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{5,})/g;
    return regex.test(str)
}

function validateConfirm(){
    return (passwordField.value.trim() === confirmPassword.value.trim())
}

submitBtn.addEventListener('click', () => {
    if(!validatePassword(passwordField.value)) {
        passwordField.classList.add('invalid')
    }
    if(!validateConfirm()) {
        confirmPassword.classList.add('invalid')
    }
});

passwordField.addEventListener('oninput', () => {
    if(!validatePassword(passwordField.value)) {
        passwordField.classList.add('invalid')
    }else{
        passwordField.classList.remove('invalid')
    }
});
confirmPassword.addEventListener('oninput', () => {
    if(!validateConfirm()) {
        confirmPassword.classList.add('invalid')
    }else{
        confirmPassword.classList.remove('invalid')
    }
});