let submitBtn = document.querySelector('.registr-submit__btn')
let dateField = document.getElementById('Date');
function validateData(str) {
    let re =/\d+-(0\d|1[0-2])-([0-2]\d|3[0-1])/g;
    return re.test(str)
}

submitBtn.addEventListener('click', () => {
    if(!validateData(dateField.value)) {
        dateField.classList.add('invalid')
    }
})

dateField.addEventListener('blur', () => {
    if(!validateData(dateField.value)) {
        dateField.classList.add('invalid')
    }
})
dateField.addEventListener('focus', () => {
    if(dateField.classList.contains('invalid')) {
        dateField.classList.remove('invalid')
    }
});

